vpc_id      = "vpc-018e0383115265e06"
image_regex = "*ubuntu-jammy-22.04-amd64-server*"

polkadot_instances = {
  node1 = {
    instance_type     = "c6i.4xlarge"
    availability_zone = "us-east-1a"
    subnet_id         = "subnet-007e87403e78ac59f"
    root_block_device = [
      {
        encrypted   = true
        volume_type = "gp2"
        volume_size = 1000
      }
    ]
  }
  node2 = {
    instance_type     = "c6i.4xlarge"
    availability_zone = "us-east-1b"
    subnet_id         = "subnet-0277aa5420abcc3fa"
    root_block_device = [
      {
        encrypted   = true
        volume_type = "gp2"
        volume_size = 1000
      }
    ]
  }
}

