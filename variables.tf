variable "vpc_id" {
  description = "The VPC ID"
  type        = string
}
variable "polkadot_instances" {
  description = "A map of multiple maps, each map represent one node, the options inside each node map are: instance_type(string), availabilty_zone(string), subnet_id(string), root_block_device(list(map))"
  type        = map(any)
}

variable "image_regex" {
  description = "The image name pattern"
  type        = string
}

