data "aws_ami" "polkadot" {
  most_recent = true
  filter {
    name   = "name"
    values = [var.image_regex]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}