output "polkadot_ec2" {
  description = "full output to created instances"
  value       = module.polkadot_ec2
}

output "private_key_pem" {
  description = "Private key data in PEM (RFC 1421) format"
  value       = try(trimspace(module.polkadot_key_pair.private_key_pem), "")
  sensitive   = true
}

output "public_key_pem" {
  description = "Public key data in PEM (RFC 1421) format"
  value       = try(trimspace(module.polkadot_key_pair.public_key_pem), "")
  sensitive   = true
}