# AWS Polkadot nodes

This repo have the TF needed to create nodes to run Polkadot EC2 instances.


## Requirements
1- AWS Account

2- VPC

3- Subnets

## Procedure

Leveraging the EC2 and security group TF modules created and maintained by AWS, we will only need to define the variables for the module to create one or more EC2 instances. 
1. Authenticate with AWS, this can be achieved by creating the credentials in AWS and then populate the  environment variables.
```shell
export AWS_ACCESS_KEY_ID=your_access_key_id
export AWS_SECRET_ACCESS_KEY=your_secret_access_key
export AWS_DEFAULT_REGION=your_preferred_region
```
2. Configure Terraform state backend (**backend.tf**) to S3 bucket that's accessible from where you are running Terraform execution, you will also need to set the path and the name of the state and you will also need to set dynamodb for managing TF locks.

```h
terraform {

  backend "s3" {
    bucket = "terraform-state-backend-example"       #<------Your state bucket------>
    key    = "terraform.statetf"                     #<------The path where you want to have the state inside the bucket------>
    dynamodb_table  = "terraform-state-lock-example" #<------ Your dynamodb table------>
    region = "us-east-1"                             #<------ the region ------>
  }

}
```
3.  Fill the variables that the module need to create the EC2 instances, you will need to set the variables that controls where the EC2 instances will be deployed and these are the VPC ID, availability zones and subnet IDs, also you will need set the image name pattern, as image IDs differ from region to another, so there is a data resource that will make sure you will have the same image in whatever region you decide to run this code in.

    These variables should be set in a file with a name `*.auto.tfvars` where the asterisk `*` could be anything and Terraform will pick it up.
```h
vpc_id      =  "vpc-018e0383115265e06"              # <-- Your VPC ID -->
image_regex =  "*ubuntu-jammy-22.04-amd64-server*"
  
polkadot_instances = {
  node1 = {
    instance_type     = "c6i.4xlarge"
	availability_zone =  "us-east-1a"               # <-- Your availability zone -->
	subnet_id         =  "subnet-007e87403e78ac59f" # <-- Your subnet ID-->
	root_block_device = [
	  {
	    encrypted   =  true
		volume_type =  "gp2"
		volume_size = 1000
	  }
    ]
  }
    
  node2 = {
    instance_type     = "c6i.4xlarge"
    availability_zone =  "us-east-1b"               # <-- Your availability zone -->
    subnet_id         =  "subnet-0277aa5420abcc3fa" # <-- Your subnet ID-->
	root_block_device = [
	  {
		encrypted =  true
		volume_type =  "gp2"
		volume_size = 1000
      }
    ]
  }
}
```