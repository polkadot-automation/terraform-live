module "polkadot_ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "5.3.1"

  for_each = var.polkadot_instances

  name = "polkadot-${each.key}"

  instance_type          = each.value.instance_type
  ami                    = data.aws_ami.polkadot.image_id
  key_name               = module.polkadot_key_pair.key_pair_name
  availability_zone      = each.value.availability_zone
  subnet_id              = each.value.subnet_id
  vpc_security_group_ids = [module.polkadot_ec2_sg.security_group_id]

  enable_volume_tags = false
  root_block_device  = lookup(each.value, "root_block_device", [])

}


module "polkadot_key_pair" {
  source = "terraform-aws-modules/key-pair/aws"
  version = "2.0.2"
  key_name           = "polkadot-key"
  create_private_key = true
}


module "polkadot_ec2_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.1.0"

  name   = "polkadot-sg"
  vpc_id = var.vpc_id
  ingress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]

  egress_with_cidr_blocks = [
    {
      rule        = "https-443-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 123
      to_port     = 123
      protocol    = "udp"
      description = "NTP"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

}