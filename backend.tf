terraform {

  backend "s3" {
    bucket = "terraform-state-backend-example"       #<------set this to a valid bucket------>
    key    = "terraform.statetf"                     #<------set this to the path where you want to have the state inside the bucket------>
    dynamodb_table  = "terraform-state-lock-example" #<------set this to dynamodb table------>
    region = "us-east-1"                             #<------set this to the region ------>
  }

}

